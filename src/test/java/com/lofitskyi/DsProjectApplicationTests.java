package com.lofitskyi;

import com.lofitskyi.entity.GeneralReport;
import com.lofitskyi.service.SupplierService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
@TestPropertySource(locations= "classpath:application_test.yml")
public class DsProjectApplicationTests {

	@Autowired
	private SupplierService supplierService;

	@Test
	public void contextLoads() throws IOException {
		final GeneralReport iphone = supplierService.getItemWithRisingPrice();

		System.out.println(iphone);
	}



}
