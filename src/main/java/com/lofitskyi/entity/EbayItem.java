package com.lofitskyi.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "ebay_items")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class EbayItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String productId;
    private String price;
    private String shippingCost;
    private String name;
    private String link;
    private String imgLink;

    public EbayItem(String productId, String price, String shippingCost, String name, String link, String imgLink) {
        this.productId = productId;
        this.price = price;
        this.shippingCost = shippingCost;
        this.name = name;
        this.link = link;
    }
}
