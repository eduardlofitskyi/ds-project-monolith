package com.lofitskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "listings")
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class Listing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    private EbayItem item;
    private String sku;
    private String price;
    private Boolean active;

    public Listing(EbayItem item, String sku, String price, Boolean active) {
        this.item = item;
        this.sku = sku;
        this.price = price;
        this.active = active;
    }
}
