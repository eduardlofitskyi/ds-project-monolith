package com.lofitskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class GeneralReport {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    private Collection<PriceReport> priceReports;

    private LocalDateTime timestamp;

    public GeneralReport(Collection<PriceReport> priceReports, LocalDateTime timestamp) {
        this.priceReports = priceReports;
        this.timestamp = timestamp;
    }
}
