package com.lofitskyi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class PriceReport {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @OneToOne
    private EbayItem item;

    private String oldPrice;
    private String newPrice;

    public PriceReport(EbayItem item, String oldSupplierPrice, String newSupplierPrice) {
        this.item = item;
        this.oldPrice = oldSupplierPrice;
        this.newPrice = newSupplierPrice;
    }
}
