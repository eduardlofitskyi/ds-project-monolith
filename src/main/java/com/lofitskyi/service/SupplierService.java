package com.lofitskyi.service;

import com.lofitskyi.entity.EbayItem;
import com.lofitskyi.entity.GeneralReport;

import java.io.IOException;
import java.util.List;

public interface SupplierService {
    GeneralReport getItemWithRisingPrice() throws IOException;
    List<EbayItem> getItemsByLink(String link);
}
