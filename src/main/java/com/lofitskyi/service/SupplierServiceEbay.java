package com.lofitskyi.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lofitskyi.entity.EbayItem;
import com.lofitskyi.entity.GeneralReport;
import com.lofitskyi.entity.PriceReport;
import com.lofitskyi.repository.EbayItemRepository;
import com.lofitskyi.utils.UrlUtils;
import converter.JsonToEbayItem;
import converter.JsonToEntity;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpMethod.GET;

@Service
public class SupplierServiceEbay implements SupplierService {

    private static final String SEARCH_ITEM_URL = "https://api.ebay.com/buy/browse/v1/item_summary/search?&q=";
    private static final String GET_ITEM_URL = "https://api.ebay.com/buy/browse/v1/item/";

    @Autowired
    private EbayItemRepository itemRepository;

    @Autowired
    private RestTemplate restTemplate;

    private JsonToEntity<List<EbayItem>> jsonConverter = new JsonToEbayItem();

    private String accessToken = "v^1.1#i^1#r^0#p^1#I^3#f^0#t^H4sIAAAAAAAAAOVXW2wUVRju7LY1gMBLo4TUZh0wIjqzZ3Z29jJ21yy90MZCC9tWaCF1dubMMnR3ZpxzhrIGk6UoaBC5vRAk0iiYKAHhoSIPFhA1gk9Kgg8mJoIQpCTGS1ATNJ6ZXcq2Eq5FSNyXzfznP//5vu///3MB+coJs9c2rf19MvWAZyAP8h6K4iaBCZUVT07xeqZXlIESB2ogPzNf3u89X4ukbMYUF0JkGjqCvpXZjI5E1xijbUsXDQlpSNSlLEQilsVkYl6LGGCBaFoGNmQjQ/ua62N0iJdBKCAJAkil1JSqEqt+JWa7EaMFKMuRCB8KQC4YjaZkMo6QDZt1hCUdx+gA4MIMCDEg0M4FRCCIAs8KEb6L9nVCC2mGTlxYQMdduKI71yrBen2oEkLQwiQIHW9ONCZbE831DfPba/0lseJFHZJYwjYa/VVnKNDXKWVseP1lkOstJm1ZhgjR/nhhhdFBxcQVMLcB35U6CDkQDfJRnuPCgsSFx0XKRsPKSvj6OByLpjCq6ypCHWs4dyNFiRqp5VDGxa/5JERzvc/5W2BLGU3VoBWjG+YkFifa2uh4g2JLltJiMASOpmNoMW0L6xkIolyQ4yMcI/EqVPkIKC5UiFaUecxKdYauaI5oyDffwHMgQQ1Ha8OJQok2xKlVb7USKnYQlfoFr2gYDnY5SS1k0cbLdCevMEuE8LmfN87AyGyMLS1lYzgSYeyAK1GMlkxTU+ixg24tFstnJYrRyzA2Rb+/r6+P7eNZw0r7AwBw/kXzWpLyMpiVaOLr9HrBX7vxBEZzqciQzESaiHMmwbKS1CoBoKfpeCAoRLlwUffRsOJjrf8ylHD2j+6I8eoQWVaEUApGeEmRwwoIjkeHxItF6ndwwJSUY7KS1QuxmZFkyMikzuwstDRF5AU1wEdUyCihqMoEo6rKpAQlxHAqhADCVEqORv5PjXKzpZ6UDRO2GRlNzo1LwY9bsfOW0iZZOJeEmQwx3GzVX5MkckjedXpOr98SRScGIkEkU2Od2mZlI+s3JLKpOaYeF/Ud8dbIeXhfJZUQLDDVlMJBxrp0WbRCZi2IDNsiZzjb6uzr7UYv1EmXYMvIZKDVyd2REuO3o9+j3fyarOSMRmTsud+Y3eI2eZu1LeF7yLq8n+q+BnNyyw4FgcAJ/B1xq3Pz2p77DzatW0psk4EwVO7CBcQ/+jkUL3N/XD81CPqpA+RFBfzgMW4GeLTS21HufXA60jBkNUllkZbWyS3fgmwvzJmSZnkqqe7q/e/1lDzABpaCaSNPsAleblLJewxUXx2p4KY+PJkLgxAIcAEgCHwXmHF1tJx7qLzqg5aF61cNrntjZ9+LF5ZOPbXmgjHRBJNHnCiqooxURtng0CPPHN077OXmLh7uCn295vAPe45fPtv3RZU/PvPIE+C5/Z1zj5XXDK37cPuvr6Q9nr+OyQennbYOM5tf2/1Zx9b89zWb9ixaMVxR+/HpyxvPfXpxyakXvto3q6zq8TOrD/yZ3Nxa83T1b39Xx9bb9qWumWeXfJJ/dwvVYm5cnkj//NO3TZ3LT8/Ze65nwzdynedcw4Xa5NCRnVE2fSIQObmb2icNHbr0/kXfm1vbBGGH90xzpGH7loZVXTVVP9K/nH/W6OzeEfx8iziJOTlllrnpLXXXhkPs8VONJ76Ez2tHa/due6rnuz+24Zf2vbpoeOJH/W/vijW+vGb1wdmbF9Skj3a3rqAGOnrfaXq9kL5/ANO++m0aDwAA";

    @Override
    public GeneralReport getItemWithRisingPrice() throws IOException {

        final List<EbayItem> items = itemRepository.findAll();

        return getReportByPrice(items);
    }

    @Override
    public List<EbayItem> getItemsByLink(String link) {
        HttpEntity<String> entity = establishHeaders();

        String productName = UrlUtils.getProductNameFromUrl(link);

        final ResponseEntity<String> exchange;

        String condition = "{NEW}";

        exchange = restTemplate.exchange(SEARCH_ITEM_URL + productName + "&limit=15&filter=conditions:{condition}", GET, entity, String.class, condition);

        return jsonConverter.convert(exchange.getBody());
    }

    private GeneralReport getReportByPrice(List<EbayItem> items) throws IOException {
        List<PriceReport> risingItems = new ArrayList<>();
        HttpEntity<String> entity = establishHeaders();

        for (EbayItem item : items) {
            final ResponseEntity<String> exchange = restTemplate.exchange(GET_ITEM_URL + item.getProductId(), GET, entity, String.class);
            String json = exchange.getBody();

            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(json);
            Double price = jsonNode.get("price").get("value").asDouble();
            Double shippingCost = jsonNode.get("shippingOptions").get(0).get("shippingCost").get("value").asDouble();

            BigDecimal actualPrice = asBigDecimal(price).add(asBigDecimal(shippingCost));
            BigDecimal dbPrice = asBigDecimal(item.getPrice());

            if (actualPrice.compareTo(dbPrice) != 0) {
                risingItems.add(new PriceReport(item, item.getPrice(), price.toString()));
            }
        }

        return new GeneralReport(risingItems, LocalDateTime.now());
    }

    private HttpEntity<String> establishHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " + accessToken);
        headers.add("X-EBAY-C-ENDUSERCTX", "contextualLocation=country%3DUS%2Czip%3D90040");

        return new HttpEntity<>("parameters", headers);
    }

    private BigDecimal asBigDecimal(String value) {
        return BigDecimal.valueOf(Double.parseDouble(value));
    }

    private BigDecimal asBigDecimal(Double value) {
        return BigDecimal.valueOf(value);
    }
}
