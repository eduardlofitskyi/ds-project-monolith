package com.lofitskyi.repository;

import com.lofitskyi.entity.EbayItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource(collectionResourceRel = "ebay_item", path = "ebay_item", exported = false)
public interface EbayItemRepository extends JpaRepository<EbayItem, Long>{
}
