package com.lofitskyi.repository;

import com.lofitskyi.entity.Listing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource(collectionResourceRel = "listing", path = "listing")
public interface ListingRepository extends JpaRepository<Listing, Long>{

    List<Listing> findAllByActiveTrue();
}
