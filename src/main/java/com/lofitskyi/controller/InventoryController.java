package com.lofitskyi.controller;

import com.lofitskyi.entity.Listing;
import com.lofitskyi.repository.ListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/inventory")
public class InventoryController {

    @Autowired
    private ListingRepository repository;

    @GetMapping
    public List<Listing> getAllActiveFromInventory() {
        return repository.findAllByActiveTrue();
    }
}
