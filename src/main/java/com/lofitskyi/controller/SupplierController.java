package com.lofitskyi.controller;

import com.lofitskyi.entity.EbayItem;
import com.lofitskyi.entity.GeneralReport;
import com.lofitskyi.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @GetMapping(value = "/price/up")
    public ResponseEntity<GeneralReport> generateReportWithGrowthPrice(){

//        final GeneralReport itemWithRisingPrice = supplierService.getItemWithRisingPrice();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/link")
    public ResponseEntity<List<EbayItem>> getProductsByLink( @RequestParam(value = "link", required = false) String link){
        final List<EbayItem> itemByLink = supplierService.getItemsByLink(link);
        return new ResponseEntity<>(itemByLink, HttpStatus.OK);
    }
}
