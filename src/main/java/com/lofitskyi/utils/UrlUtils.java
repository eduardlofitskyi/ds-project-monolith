package com.lofitskyi.utils;

import org.apache.commons.lang3.StringUtils;

public class UrlUtils {

    public static final String EBAY_ITEM_URL = "http://www.ebay.com/itm/";

    public static String getProductNameFromUrl(String url){
        String s = StringUtils.removeIgnoreCase(url, EBAY_ITEM_URL);
        s = s.split("/")[0];
        s = StringUtils.replaceAll(s, "-", StringUtils.SPACE);
        s = StringUtils.removeEnd(s, StringUtils.SPACE);
        return s;
    }
}
