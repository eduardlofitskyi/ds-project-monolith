package com.lofitskyi.config;

import com.lofitskyi.entity.EbayItem;
import com.lofitskyi.entity.Listing;
import com.lofitskyi.repository.EbayItemRepository;
import com.lofitskyi.repository.ListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@Profile("dev")
public class DevConfig {

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2).build();
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter bean = new HibernateJpaVendorAdapter();
        bean.setDatabase(Database.H2);
        bean.setGenerateDdl(true);
        return bean;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(dataSource);
        bean.setJpaVendorAdapter(jpaVendorAdapter);
        bean.setPackagesToScan("com.lofitskyi");
        return bean;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    class StartUpActions{

        @Autowired
        private EbayItemRepository ebayItemRepository;

        @Autowired
        private ListingRepository listingRepository;

        @Bean
        public CommandLineRunner runner(){
            return (args) -> {
                EbayItem item = ebayItemRepository.save(new EbayItem("v1|152290355366|0", "74.95", "0", "White Light House Ceramic Tile Mural 6 of 6 Lighthouse Backsplash Kiln Fired", "http://www.ebay.com/itm/White-Light-House-Ceramic-Tile-Mural-6-of-6-Lighthouse-Backsplash-Kiln-Fired-/152290355366?hash=item23753668a6:g:CSkAAOxy7nNTSBGL", "http://em.com"));
                listingRepository.save(new Listing(item, "SKU#1", "25.50", Boolean.TRUE));
                listingRepository.save(new Listing(item, "SKU#1", "25.50", Boolean.TRUE));
            };
        }

    }
}
