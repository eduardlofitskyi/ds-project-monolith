package converter;

public interface JsonToEntity<T> {
    T convert(String json);
}
