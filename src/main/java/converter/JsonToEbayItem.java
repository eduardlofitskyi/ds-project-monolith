package converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lofitskyi.entity.EbayItem;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JsonToEbayItem implements JsonToEntity<List<EbayItem>> {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public List<EbayItem> convert(String json) {
        JsonNode jsonNode = null;

        try {
            jsonNode = mapper.readTree(json);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<EbayItem> items = new LinkedList<>();

        int total = jsonNode.get("limit").asInt();

        for (int i = 0; i < total; i++) {

            JsonNode currentJsonNode = jsonNode.get("itemSummaries").get(i);

            String shippingCost;

            shippingCost = getShippingCost(currentJsonNode);

            items.add(EbayItem.builder()
                    .link(currentJsonNode.get("itemWebUrl").asText())
                    .imgLink(currentJsonNode.get("image").get("imageUrl").asText())
                    .name(currentJsonNode.get("title").asText())
                    .price(currentJsonNode.get("price").get("value").asText())
                    .shippingCost(shippingCost)
                    .productId(currentJsonNode.get("itemId").asText())
                    .build());
        }

        return items;
    }

    private String getShippingCost(JsonNode jsonNode) {
        String shippingCost;

        if (jsonNode.get("shippingOptions") == null || jsonNode.get("shippingOptions").get(0).get("shippingCostType").asText().equals("CALCULATED")) {
            shippingCost = "CALCULATED";
        } else {
            shippingCost = jsonNode.get("shippingOptions").get(0).get("shippingCost").get("value").asText();
        }
        return shippingCost;
    }
}
